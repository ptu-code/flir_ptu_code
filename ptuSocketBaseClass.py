# -*- coding: utf-8 -*-
"""
This the low-level class for interacting with the FLIR E-series PTU
The class creates a PTU object, which can be commanded or queried
all commands are from the FLIR E Series Pan-Tilt Command Reference Manual

WARNING: (from page 61 of E-series command reference)
    E Series PTUs have a region at 180° (+/- 5°) called a
    lockout region. Powering down a PTU in this area means that it will not know which way to turn to
    return home when you power it back on. PTUs with firmware version 3.4.0 or later include a specific
    error message if you attempt a reset when the PTU is in the lockout region. Previous firmware versions
    display a more generic “axis error” message. Contact FLIR Technical Support as directed in your PTU User
    Manual for assistance if you see this lockout region error message.
"""

import socket
import time
from warnings import warn

__all__ = ['PTU']

class PTU(object):
    
    # Initiating function, is called when PTU is created, defines the PTU object
    # ipAddress of PTU, portNumber of computing device running this code
    def __init__(self, ipAddress = '10.32.40.200', portNumber = 4000):
        server_address = (ipAddress, portNumber)
        # define the socket connection type
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # try to open the socket connection
        try:
            self.sock.connect(server_address)
            print('Connection SUCCESS to PTU with IP address "' + ipAddress + '" over port number "' + str(portNumber) + '"')
        except:
            print('Connection FAILURE to PTU with IP address "' + ipAddress + '" over port number "' + str(portNumber) + '"')
            # TODO - need to do some other flag raising here? so other parts of program are aware?
            
        self._TERM = b'\r\n' # String that denotes the end of a response from the PTU
        time.sleep(1) # Need to sleep for a second. During start-up, the PTU is sending messages, but clears them after a bit
        self.__get_response() # clear the empty message that is left after startup
                
        self.send(b'ED ') # Turn 'ECHO MODE' off, so we do not recieve back the commands we send
        self.__echo = False
        
        self.__limitMode = self._determine_limit_mode()
        self.__panResolution = self._determine_pan_resolution()
        self.__tiltResolution = self._determine_tilt_resolution()
        self.__minPan = self._determine_min_pan()
        self.__maxPan = self._determine_max_pan()
        self.__minTilt = self._determine_min_tilt()
        self.__maxTilt = self._determine_max_tilt()
        self.__userMinPan, self.__userMaxPan, self.__userMinTilt, self.__userMaxTilt = self._determine_user_limits()
        self.__maxPanSpeed = self._determine_max_pan_speed()
        self.__maxTiltSpeed = self._determine_max_tilt_speed()
        self.__controlMode = self._determine_control_mode()
        self.__executionMode = self._determine_execution_mode()
        self.__panStepMode = self._determine_pan_step_mode()
        self.__tiltStepMode = self._determine_tilt_step_mode()
        self.__contRotate = self._determine_continuous_rotation_mode()
        
    def __del__(self): # halts the PTU and closes the socket connection
        self.halt()
        self.sock.close()

    def __send_command(self, command): # sends command to socket
        if isinstance(command, str): # if command is a string, encode to bytes
            command = command.encode()
        assert isinstance(command, bytes) # double check that command is bytes
        self.sock.sendall(command) # send command
        
    def __get_response(self): # reads all data in waiting from PTU up until the self._Term
        # taken from here: code.activestate.com/recipes/408859-socketrecv-three-ways-to-turn-it-into-recvall/
        total_data=[]; data=''
        while True:
            data = self.sock.recv(8192)
            if self._TERM in data:
                total_data.append(data[:data.find(self._TERM)])
                break
            total_data.append(data)
            if len(total_data)>1:
                #check if end_of_data was split
                last_pair=total_data[-2]+total_data[-1]
                if self._TERM in last_pair:
                    total_data[-2]=last_pair[:last_pair.find(self._TERM)]
                    total_data.pop()
                    break
        return b''.join(total_data)
    
##############################################################################
# Send command function
        
    def send(self, command):
        command = command.upper() # Changes all inputs to upper case

        # Convert command to bytes, check for valid terminating character, which is a blank space
        if isinstance(command, str):
            if command[-1] != ' ':
                raise ValueError("Commands must end with a space (' ')")
            command = command.encode()
        elif isinstance(command, bytes):
            if command.decode()[-1] != ' ':
                raise ValueError("Commands must end with a space (' ')")

        self.__send_command(command)
        return self.__get_response().decode()

###############################################################################
# Various properties of the PTU that are called upon initialization, but may get updated later on
        
    def _determine_pan_resolution(self): # Returns panning resolution in arcseconds per position
        response = self.send(b'PR ')
        # Response is      b'* 23.142857 seconds arc per position'
        return float(response.split()[1]) 
        # Split breaks the response string into seperate words, dilineated by blank spaces
        # [1] is the 2nd element, which is the value we want. [0] is '*', which the PTU puts at the front of every returned message

    def _determine_tilt_resolution(self): # Returns tilting resolution in arcseconds per position
        response = self.send(b'TR ')
        return float(response.split()[1])

    def _determine_min_pan(self): # Returns minimum pan angle set on the PTU [degrees]
        response = self.send(b'PN ')
        return self.panPositionToAngle(int(response.split()[5]))

    def _determine_max_pan(self): # Returns maximum pan angle set on the PTU [degrees]
        response = self.send(b'PX ')
        return self.panPositionToAngle(int(response.split()[5]))

    def _determine_min_tilt(self): # Returns minimum tilt angle set on the PTU [degrees]
        response = self.send(b'TN ')
        return self.tiltPositionToAngle(int(response.split()[5]))

    def _determine_max_tilt(self): # Returns maximum tilt angle set on the PTU [degrees]
        response = self.send(b'TX ')
        return self.tiltPositionToAngle(int(response.split()[5]))

    def _determine_user_limits(self): # Returns a tuple of user-defined limits -> (minPan, maxPan, minTilt, maxTilt)
        # NOTE: This will return the user-defined limits, but does not mean user defined limits are currently being enforced!!!
        minPan = self.panPositionToAngle(int(self.send(b'PNU ').split()[5]))
        maxPan = self.panPositionToAngle(int(self.send(b'PXU ').split()[5]))
        minTilt = self.tiltPositionToAngle(int(self.send(b'TNU ').split()[5]))
        maxTilt = self.tiltPositionToAngle(int(self.send(b'TXU ').split()[5]))
        return minPan, maxPan, minTilt, maxTilt

    def _determine_max_pan_speed(self): # Returns maximum panning speed set on the PTU [deg/sec]
        response = self.send(b'PU ')
        return self.panPositionToAngle(int(response.split()[5]))

    def _determine_max_tilt_speed(self): # Returns maximum tilting speed set on the PTU [deg/sec]
        response = self.send(b'TU ')
        return self.tiltPositionToAngle(int(response.split()[5]))

    def _determine_control_mode(self): # Returns current control mode ('pos' or 'vel')
        response = self.send(b'C ')
        if response.split()[1].lower() == "independent":
            return 'pos'
        else:
            return 'vel'
        
    def _determine_execution_mode(self): # Returns current execution mode ('immediate' or 'slaved')
        response = self.send(b'IQ ')
        if response.split()[1].lower() == "immediate":
            return 'immediate' # In this mode, commands are executed immediately
        else:
            return 'slaved' # In this mode, commands are executed only after receiveing an 'await' command
    
    def _determine_pan_step_mode(self): # Returns pan step mode
        # Can be full [F], half [H], quarter [Q], eigth [E], or auto [A]
        response = self.send(b'WP ')
        return response.split()[5]
    
    def _determine_tilt_step_mode(self): # Returns tilt step mode
        # Can be full [F], half [H], quarter [Q], eigth [E], or auto [A]
        response = self.send(b'WT ')
        return response.split()[5]
    
    def _determine_limit_mode(self): # Returns currently enforced limit mode ('user', 'factory', or 'disabled')
        response = self.send(b'L ')
        if response.split()[1].lower() == "user":
            return 'user'
        elif response.split()[1].lower() == "factory":
            return 'factory'
        elif response.split()[2].lower() == "disabled":
            return 'limits disabled'
        
    def _determine_continuous_rotation_mode(self): # Returns wether continuous rotation mode is 'enabled' or 'disabled'
        response = self.send(b'PC ')
        if response.split()[1].lower() == "disabled":
            return 'disabled'
        elif response.split()[1].lower() == "enabled":
            return 'enabled'
        
    
###############################################################################
# Property functions, can be called

    @property
    def panResolution(self): # Panning resolution in arcseconds per position
        return self.__panResolution
    @property
    def tiltResolution(self): # Tilting resolution in arcseconds per position
        return self.__tiltResolution
    @property
    def minPan(self): # Minimum factory set pan position
        return self.__minPan
    @property
    def maxPan(self): # Maximum factory set pan position
        return self.__maxPan
    @property
    def minTilt(self): # Minimum factory set tilt position
        return self.__minTilt
    @property
    def maxTilt(self): # Maximum factory set tilt position
        return self.__maxTilt
    @property
    def maxPanSpeed(self): # Maximum possible panning target/operational speed
        return self.__maxPanSpeed
    @property
    def maxTiltSpeed(self): # Maximum possible tilting target/operational speed
        return self.__maxTiltSpeed
    @property
    def userMinPan(self): # Minimum user set pan position
        return self.__userMinPan
    @property
    def userMaxPan(self): # Maximum user set pan position
        return self.__userMaxPan
    @property
    def userMinTilt(self): # Minimum user set tilt position
        return self.__userMinTilt
    @property
    def userMaxTilt(self): # Maximum user set tilt position
        return self.__userMaxTilt
    @property
    def controlMode(self): # Control mode ('pos' or 'vel')
        return self.__controlMode
    @property
    def executionMode(self): # Execution mode ('immediate' or 'slaved')
        return self.__executionMode
    @property
    def firmwareVersion(self): # Firmware version on PTU
        return self.send(b'V ').split()[3]
    @property
    def stepMode(self): # Eighth, Quarter, or Half
        return (self.__panStepMode, self.__tiltStepMode)
    @property
    def limitMode(self): # user or factory
        return self.__limitMode
    @property
    def contRotate(self): # enabled or disabled
        return self.__contRotate
##############################################################################
# Angle and Position Conversions
    
    def panAngleToPosition(self, angle): # Convert an angle (in degrees) to a number of pan positions
        return round(angle / (self.panResolution / 3600))

    def tiltAngleToPosition(self, angle): # Convert an angle (in degrees) to a number of tilt positions
        return round(angle / (self.tiltResolution / 3600))
    
    def panPositionToAngle(self, position): # Convert a number of positions to a pan angle (in degrees)
        return (position * (self.panResolution / 3600))

    def tiltPositionToAngle(self, position): # Convert a number of positions to a pan angle (in degrees)
        return (position * (self.tiltResolution / 3600))
    
###############################################################################
# Setting limits, execution modes, etc....
            
    def setUserPanLimits(self, minimum_deg, maximum_deg): # Set user limits on min and max pan position, input in degrees
        minimum = self.panAngleToPosition(minimum_deg)
        maximum = self.panAngleToPosition(maximum_deg)
        resp = self.send(b'PNU' + str(int(minimum)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan minumum limit response: ' + resp, RuntimeWarning)
            ret = False
        resp = self.send(b'PXU' + str(int(maximum)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan maximum limit response: ' + resp, RuntimeWarning)
            ret = False
        else:
            ret = 'User min and max pan limits set to ' + str(int(minimum_deg)) + ' and ' + str(int(maximum_deg))
            self.__userMinPan, self.__userMaxPan, self.__userMinTilt, self.__userMaxTilt = self._determine_user_limits()
        return ret

    def setUserTiltLimits(self, minimum_deg, maximum_deg): # Set user limit on min and max tilt positions, input in degrees
        minimum = self.tiltAngleToPosition(minimum_deg)
        maximum = self.tiltAngleToPosition(maximum_deg)
        resp = self.send(b'TNU' + str(int(minimum)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt minumum limit response: ' + resp, RuntimeWarning)
            ret = False
        resp = self.send(b'TXU' + str(int(maximum)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt maximum limit response: ' + resp, RuntimeWarning)
            ret = False
        else:
            ret = 'User min and max pan limits set to ' + str(int(minimum_deg)) + ' and ' + str(int(maximum_deg))
            self.__userMinPan, self.__userMaxPan, self.__userMinTilt, self.__userMaxTilt = self._determine_user_limits()
        return ret

    def enableUserLimits(self): # Enable user limits if not already enabled
            self.send(b'LU ')
            self.resetPTU() # WARNING: PTU needs to be reset to enforce new limits
            self.__limitMode = self._determine_limit_mode()

    def disableUserLimits(self): # Disable user limits, enable factory limits
            self.send(b'LE ')
            self.resetPTU() # WARNING: PTU needs to be reset to enforce new limits
            self.__limitMode = self._determine_limit_mode()
            
    def disableAllLimits(self): # Disable all limits
            self.send(b'LD ')
            self.resetPTU() # WARNING: PTU needs to be reset to enforce new limits
            self.__limitMode = self._determine_limit_mode()
            
    def setExecutionMode(self, mode): # Change execution mode between Instantaneous (I) and Slaved (S)
        if not isinstance(mode, str):
            raise TypeError('Execution mode must be a string "I" or "S".')
        if mode == 'I': # PTU moves immediately after command is sent, and the script continues. Can use Await to pause script
            resp = self.send(b'I ')
        elif mode == 'S': # Must use Await. Script pauses until PTU finishes its command
            resp = self.send(b'S ')
        else:
            raise ValueError('Execution mode must be a string "I" or "S".')
        if resp[0] != '*':
            warn('Execution mode response: ' + resp, RuntimeWarning)
            return False
        else:
            self.__executionMode = self._determine_execution_mode()
            return True
                
    def setPanStepMode(self, mode): # Change the pan step mode to full [F], half [H], quarter [Q], eigth [E], or auto [A]
        if not isinstance(mode, str):
            raise TypeError('Control mode must be a string "F", "H", "Q", "E", or "A".')
        if mode == 'F':
            resp = self.send(b'WPF ')
        elif mode == 'H':
            resp = self.send(b'WPH ')
        elif mode == 'Q':
            resp = self.send(b'WPQ ')
        elif mode == 'E':
            resp = self.send(b'WPE ')
        elif mode == 'A':
            resp = self.send(b'WPA ')
        else:
            raise ValueError('Control mode must be a string "F", "H", "Q", "E", or "A".')
        if resp[0] != '*':
            warn('Pan step mode response: ' + resp, RuntimeWarning)
            return False
        else:
            self.__panStepMode = self._determine_pan_step_mode()
            self.__panResolution = self._determine_pan_resolution()
            self.__maxPanSpeed = self._determine_max_pan_speed()
            return True
    
    def setTiltStepMode(self, mode): # Change the tilt step mode to full [F], half [H], quarter [Q], eigth [E], or auto [A]
        if not isinstance(mode, str):
            raise TypeError('Control mode must be a string "F", "H", "Q", "E", or "A".')
        if mode == 'F':
            resp = self.send(b'WTF ')
        elif mode == 'H':
            resp = self.send(b'WTH ')
        elif mode == 'Q':
            resp = self.send(b'WTQ ')
        elif mode == 'E':
            resp = self.send(b'WTE ')
        elif mode == 'A':
            resp = self.send(b'WTA ')
        else:
            raise ValueError('Control mode must be a string "F", "H", "Q", "E", or "A".')
        if resp[0] != '*':
            warn('Tilt step mode response: ' + resp, RuntimeWarning)
            return False
        else:
            self.__tiltStepMode = self._determine_tilt_step_mode()
            self.__tiltResolution = self._determine_tilt_resolution()
            self.__maxTiltSpeed = self._determine_max_tilt_speed()
            return True
        
    def setStepMode(self, panmode, tiltmode): # Combination of the two above functions for convenience
        resp1, resp2 = self.setPanStepMode(panmode), self.setTiltStepMode(tiltmode)
        self.resetAxes()
        return resp1, resp2
    
    def setControlMode(self, mode): # Set PTU to either position or velocity control mode
        if not isinstance(mode, str):
            raise TypeError('Control mode must be a string "pos" or "vel"')
        if mode.lower() == 'pos':
            self.send(b'CI ')
            self.__controlMode = self._determine_control_mode()
        elif mode.lower() == 'vel':
            self.send(b'CV ')
            self.__controlMode = self._determine_control_mode()
        else:
            raise ValueError('Control mode must be "pos" or "vel"')
            
    def setContRotation(self, mode): # Turn on or off continous pan operation
        if not isinstance(mode, str):
            raise TypeError('Continuous rotation mode must be a string "on" or "off"')
        if mode.lower() == 'on':
            self.send(b'PCE ')
            self.__contRotate = self._determine_continuous_rotation_mode()
            self.resetAxes()
        elif mode.lower() == 'off':
            self.send(b'PCD ')
            self.__contRotate = self._determine_continuous_rotation_mode()
            self.resetAxes()
        else:
            raise ValueError('Continuous rotation mode must be a string "on" or "off"')
            
    def getContRotation(self): # query whether continuous rotation is currently on or off
        resp = self.send(b'PC ')
        return resp
            
    def resetFactoryDefaults(self): # Reset all settings to factory default
        resp = self.send(b'DF ')
        return resp
    
    def saveSettings(self): # Once user has changed settings, save them so they remain when PTU is turned off
        resp = self.send(b'DS ')
        return resp
    
    def setPanMovingPower(self, level): # Set pan moving power to low or regular (high is possible but not an option here)
        if not isinstance(level, str):
            raise TypeError('Power level must be a string, "low" or "regular"')
            return False
        if level.lower() == 'low':
            self.send(b'PML ')
            return True
        elif level.lower() == 'regular':
            self.send(b'PMR ')
            return True
        else :
            raise ValueError('Power level must be a string, "low" or "regular"')
            return False
    
    def setTiltMovingPower(self, level): # Set tilt moving power to low or regular (high is possible but not an option here)
        if not isinstance(level, str):
            raise TypeError('Power level must be a string, "low" or "regular"')
            return False
        if level.lower() == 'low':
            self.send(b'TML ')
            return True
        elif level.lower() == 'regular':
            self.send(b'TMR ')
            return True
        else :
            raise ValueError('Power level must be a string, "low" or "regular"')
            return True
    
    def setMovingPower(self, panlevel, tiltlevel): # Combination of the above two functions for convenience
        return self.setPanMovingPower(panlevel), self.setTiltMovingPower(tiltlevel)
    
    def getMovingPower(self): # Return the currently set pan and tilt moving power
        return self.send(b'PM '), self.send(b'TM ')
    
    def setResetSpeed(self, panspeed, tiltspeed): # Speed at which PTU moves when reset
        pan_speed = self.panAngleToPosition(panspeed)
        tilt_speed = self.tiltAngleToPosition(tiltspeed)
        resp1 = self.send(b'RPS' + str(int(pan_speed)).encode() + b' ')
        resp2 = self.send(b'RTS' + str(int(tilt_speed)).encode() + b' ')
        return resp1, resp2
    
###############################################################################
# Speed and acceleration parameter setting
        
    def setPanTargetSpeed(self, speed_deg): # Set a target/operational panning speed (deg/sec)
        speed = self.panAngleToPosition(speed_deg)
        resp = self.send(b'PS' + str(int(speed)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan speed response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setTiltTargetSpeed(self, speed_deg): # Set a target/operational tilting speed (deg/sec)
        speed = self.tiltAngleToPosition(speed_deg)
        resp = self.send(b'TS' + str(int(speed)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt speed response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setTargetSpeed(self, pan, tilt): # Set a target/operational panning and tilting speed (deg/sec)
        return self.setPanTargetSpeed(pan), self.setTiltTargetSpeed(tilt)
    
    def getTargetPanSpeed(self): # Returns currently set target/operational speed at which PTU should pan (deg/sec)
        resp = self.send(b'PS ')
        return self.panPositionToAngle(int(resp.split()[5]))
    
    def getTargetTiltSpeed(self): # Returns currently set target/operational speed at which PTU should tilt (deg/sec)
        resp = self.send(b'TS ')
        return self.tiltPositionToAngle(int(resp.split()[5]))

    def getTargetSpeed(self):  # Returns currently set target/operational speed at which PTU should pan and tilt (deg/sec)
        return self.getTargetPanSpeed(), self.getTargetTiltSpeed()

    def setPanAcceleration(self, acceleration_deg): # Set a pan acceleration (deg/sec^2)
        acceleration = self.panAngleToPosition(acceleration_deg)
        resp = self.send(b'PA' + str(int(acceleration)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan acceleration response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
        
    def setTiltAcceleration(self, acceleration_deg): # Set a pan acceleration (deg/sec^2)
        acceleration = self.tiltAngleToPosition(acceleration_deg)
        resp = self.send(b'TA' + str(int(acceleration)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt acceleration response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
    
    def setAcceleration(self, panAcceleration, tiltAcceleration): # Set pan and tilt accelerations (deg/sec^2)
        return self.setPanAcceleration(panAcceleration), self.setTiltAcceleration(tiltAcceleration)
        
    def getPanAcceleration(self): # Returns set pan acceleration (deg/sec^2)
        resp = self.send(b'PA ')
        return self.panPositionToAngle(int(resp.split()[4]))

    def getTiltAcceleration(self): # Returns set tilt acceleration (deg/sec^2)
        resp = self.send(b'TA ')
        return self.tiltPositionToAngle(int(resp.split()[4]))
        
    def getAcceleration(self): # Returns set panning and tilting accelerations (deg/sec^2)
        return self.getPanAcceleration(), self.getTiltAcceleration()
    
    def setPanBaseSpeed(self, speed_deg): # Set a base (instantaneous) pan speed (deg/sec)
        speed = self.panAngleToPosition(speed_deg)
        resp = self.send(b'PB' + str(int(speed)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan base speed offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
        
    def setTiltBaseSpeed(self, speed_deg): # Set a base (instantaneous) tilt speed (deg/sec)
        speed = self.tiltAngleToPosition(speed_deg)
        resp = self.send(b'TB' + str(int(speed)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt base speed offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
        
    def setBaseSpeed(self, panspeed, tiltspeed): # Set a base (instantaneous) pan and tilt speed (deg/sec)
        return self.setPanBaseSpeed(panspeed), self.setTiltBaseSpeed(tiltspeed)
        
    def getPanBaseSpeed(self): # Returns currently set base speed at which PTU should pan(deg/sec)
        resp = self.send(b'PB ')
        return self.panPositionToAngle(int(resp.split()[5]))
    
    def getTiltBaseSpeed(self): # Returns currently set base speed at which PTU should tilt (deg/sec)
        resp = self.send(b'TB ')
        return self.tiltPositionToAngle(int(resp.split()[5]))
    
    def getBaseSpeed(self): # Returns currently set base speed at which PTU should pan and tilt (deg/sec)
        return self.getPanBaseSpeed(), self.getTiltBaseSpeed()
    
    
##############################################################################
# Position controls
            
    def setPanPosition(self, pos_deg): # Command PTU to go to a pan angle (deg)
        pos = self.panAngleToPosition(pos_deg)
        resp = self.send(b'PP' + str(int(pos)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan position response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setTiltPosition(self, pos_deg): # Command PTU to go to a tilt angle (deg)
        pos = self.tiltAngleToPosition(pos_deg)
        resp = self.send(b'TP' + str(int(pos)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt position response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setPosition(self, pan, tilt): # Command PTU to go to a pan and tilt angle (deg)
        return self.setPanPosition(pan), self.setTiltPosition(tilt)
        
    def setPanPositionOffset(self, offset_deg): # Command PTU to move pan by a specified number of deg from current deg
        offset = self.panAngleToPosition(offset_deg)
        resp = self.send(b'PO' + str(int(offset)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan position offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setTiltPositionOffset(self, offset_deg): # Command PTU to move tilt by a specified number of deg from current deg
        offset = self.tiltAngleToPosition(offset_deg)
        resp = self.send(b'TO' + str(int(offset)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt position offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setPositionOffset(self, pan, tilt): # Command PTU to move pan and tilt by a specified number of deg from current degs
        return self.setPanPositionOffset(pan), self.setTiltPositionOffset(tilt)

    def getPanPosition(self): # Returns current pan position (deg)
        resp = self.send(b'PP ')
        return self.panPositionToAngle(int(resp.split()[5]))

    def getTiltPosition(self): # Returns current tilt position (deg)
        resp = self.send(b'TP ')
        return self.tiltPositionToAngle(int(resp.split()[5]))

    def getPosition(self): # Returns current pan and tilt positions (deg)
        return self.getPanPosition(), self.getTiltPosition()
    
    def getTargetPanPosition(self): # Returns pan position that PTU may be currently moving towards (deg)
        resp = self.send(b'PO ')
        return self.panPositionToAngle(int(resp.split()[5]))

    def getTargetTiltPosition(self): # Returns tilt position that PTU may be currently moving towards (deg)
        resp = self.send(b'TO ')
        return self.tiltPositionToAngle(int(resp.split()[5]))

    def getTargetPosition(self): # Returns pan and tilt positions that PTU may be currently moving (deg)
        return self.getTargetPanPosition(), self.getTargetTiltPosition()
    
##############################################################################
# Velocity controls  
    
    def setPanSpeedOffset(self, offset_deg): # Command PTU to pan a specified number of deg/sec faster than its currentl speed
        offset = self.panAngleToPosition(offset_deg)
        resp = self.send(b'PD' + str(int(offset)).encode() + b' ')
        if resp[0] != '*':
            warn('Pan speed offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setTiltSpeedOffset(self, offset_deg):  # Command PTU to tilt a specified number of deg/sec faster than its currentl speed
        offset = self.tiltAngleToPosition(offset_deg)
        resp = self.send(b'TD' + str(int(offset)).encode() + b' ')
        if resp[0] != '*':
            warn('Tilt speed offset response: ' + resp, RuntimeWarning)
            return False
        else:
            return True

    def setSpeedOffset(self, pan, tilt): # Command PTU to pan and tilt a specified number of deg/s faster than its currently defined speed while moving
        return self.setPanSpeedOffset(pan), self.setTiltSpeedOffset(tilt)
    
    def getCurrentPanSpeed(self): # Returns current panning speed, as in, how fast is it moving right now! (deg/sec)
        resp = self.send(b'PD ')
        return self.panPositionToAngle(int(resp.split()[5]))

    def getCurrentTiltSpeed(self): # Returns current tilting speed, as in, how fast is it moving right now! (deg/sec)
        resp = self.send(b'TD ')
        return self.tiltPositionToAngle(int(resp.split()[5]))

    def getCurrentSpeed(self): # Returns current panning and tilting speed, as in, how fast is it moving right now! (deg/sec)
        return self.getPanSpeed(), self.getTiltSpeed()
    
    
    '''
    
    When placed into velocity control mode with setControlMode('vel'),
    the commands that typically set the target speed will not directly command the PTU velocity
    the setPanTargetSpeed(), setTiltTargetSpeed(), and setTargetSpeed() commands function differently depending on the set control mode
    
    Explained further:
        when in position control mode, setTargetSpeed() determines the velocity at which the PTU will move after given a position command
        when in velocity control mode, setTargetSpeed() will instantly cause the PTU to start moving at the specified velocities
    
    '''

##############################################################################
# General Movement commands
    
    def halt(self): # Command PTU to stop moving immediately. DOES NOT OVERRIDE AWAIT COMMAND!
        resp = self.send('H ')
        return resp
    
    def Await(self): # Command PTU to wait for previously issued commands to complete. Required when in 'Slaved mode'
        resp = self.send('A ')
        return resp    
    
    def resetAxes(self): # Recalibrate where the 0 pan, 0 tilt position is
        resp = self.send(b'RE ')
        return resp
    
    def resetPTU(self): # Restart PTU
        # will perform whatever startup operations are set, and will revert to startup settings
        resp = self.send(b'R ')
        return resp
##############################################################################
# Network Stuff
    
    def setIPAddress(self, newIPAddress):
        resp = self.send(b'NI' + str(newIPAddress).encode() + b' ')
        if resp[0] != '*':
            warn('Set IP Address response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
        
    def getIPAddress(self):
        resp = self.send(b'NI' + b' ')
        return resp

    def setNetworkMode(self, networkMode):
        if networkMode == 'static':
            resp = self.send(b'NMS' + b' ')
        elif networkMode == 'dynamic':
            resp = self.send(b'NMD' + b' ')
        if resp[0] != '*':
            warn('Set network mode response: ' + resp, RuntimeWarning)
            return False
        else:
            return True
        
    def getNetworkMode(self):
        resp = self.send(b'NM' + b' ')
        return resp
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    