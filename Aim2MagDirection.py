# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 10:29:37 2019

@author: JPO
"""

import newPTUclassSocket as ptuSock
import numpy as np
# import time

# Define several constants
deg2rad = np.pi/180
rad2deg = 180/np.pi

EastOrWest = 'West'
ClockOrCounter = 'Clock'

def connect2ptu():
# Call to create the PTU object and connect via ethernet socket
# Inputs:
# Outputs:
    # MyPTU: Name of the PTU object
    MyPTU = ptuSock.PTU()
    MyPTU.setExecutionMode('I') # Set execution mode to instantaneous
    return MyPTU

def SetSpeeds(BaseSpeed, Acceleration, TargetSpeed, ptuObj):
# Set the base speed, acceleration, and target speed of the PTU
# Inputs:
    # BaseSpeed [deg/s]
    # Acceleration [deg/s^2]
    # TargetSpeed [deg/s]
    # ptuObj
# Outputs:
    ptuObj.setBaseSpeed(BaseSpeed, BaseSpeed)
    ptuObj.setAcceleration(Acceleration, Acceleration)
    ptuObj.setTargetSpeed(TargetSpeed, TargetSpeed)
    
def GetSpeeds(ptuObj):
# Get the base speed, acceleration, and target speed of the PTU
# Inputs:
# Outputs:
    # BasePanSpeed, BaseTiltSpeed [deg/s]
    # PanAcceleration, TiltAcceleration [deg/s^2]
    # TargetPanSpeed, TargetTiltSpeed [deg/s]
    return ptuObj.getBaseSpeed(), ptuObj.getAcceleration(), ptuObj.getTargetSpeed()
    
def SetPositionLimits(MinTilt, MaxTilt, MinPan, MaxPan, ptuObj):
# Set the anglar position limits of the PTU
# Inputs:
    # MinTilt [deg]
    # MaxTilt [deg]
    # MinPan [deg]
    # MaxPan [deg]
    # ptuObj [deg]
# Outputs:
    ptuObj.setUserPanLimits(MinTilt, MaxTilt)
    ptuObj.setUserTiltLimits(MinPan, MaxPan)
    ptuObj.enableUserLimits()
    
def GetPositionLimits(ptuObj):
# Get the anglar position limits of the PTU
# Inputs:
    # ptuObj: Name of the PTU object
# Outputs:
    # MinTilt, MaxTilt [deg]
    # MinPan, MaxPan [deg]
    return ptuObj.getUserPanLimits(), ptuObj.getUserTiltLimits()

def ZeroZero(ptuObj):
# Send the ptu to 0 pan, 0 tilt
# Inputs:
    # ptuObj: Name of the PTU object
    ptuObj.setPosition(0, 0)
    ptuObj.Await()

def Store(ptuObj):
# Send the ptu to 0 pan, minimum tilt
# Inputs:
    # ptuObj: Name of the PTU object
    ptuObj.setPosition(0, ptuObj.minTilt)
    ptuObj.Await()
    
def GetPosition(ptuObj):
# Get the current anglar positions of the PTU
# Inputs:
    # ptuObj: Name of the PTU object
# Outputs:
    # Pan, Tilt [deg]

    Pan, Tilt = ptuObj.getPosition()
    return Pan, Tilt

def SetPosition(Pan, Tilt, ptuObj):
# Send the ptu to 0 pan, 0 tilt
# Inputs:
    # ptuObj: Name of the PTU object
    ptuObj.setPosition(Pan, Tilt)

def OffMagPanAngle(Pan_Offset, Pan_0_Position_Mag_Yaw, EastOrWest):
    
    if EastOrWest == 'East':
        Pan_Angle = 90 - Pan_0_Position_Mag_Yaw + Pan_Offset
    elif EastOrWest == 'West':
        Pan_Angle = 270 - Pan_0_Position_Mag_Yaw - Pan_Offset
        
    while Pan_Angle > 180:
        Pan_Angle = Pan_Angle - 360
        
    while Pan_Angle < -180:
        Pan_Angle = Pan_Angle + 360
        
    return Pan_Angle
        
def PointOffMag(Tilt_Offset_From_Horizontal, Tilt_Correction, Yaw, Pan_Offset, EastOrWest, Pan_Correction, Magnetic_Declination, ptuObj):
# Send the ptu to 0 pan, 0 tilt
# Inputs:
    # Tilt_Offset_From_Horizontal: Defined tilt angle to point at, measured down from horizontal [deg]
    # Tilt_Correction: Correction for the Pitch/Roll of platform. Set = 0 if we don't care about this [deg]
    # Yaw: Angle from True North to X axis of platform, measured clockwise from above [deg]
    # Pan_Offset: Angle offset from Magnetic East or West [deg]
    # EastOrWest: 'East' or 'West' [string]
    # Pan_Correction: Pan correction due to PTU orientation with platform [deg]
    # Magnetic_Declination: Angle from True North to Magnetic North, measured clockwise from above [deg]
    # ptuObj: Name of the PTU object
    
    Tilt_Angle = Tilt_Offset_From_Horizontal + Tilt_Correction
    
    Magnetic_Yaw = Yaw - Magnetic_Declination
    
    Pan_0_Position_Mag_Yaw = Pan_Correction + Magnetic_Yaw
    
    Pan_Angle = OffMagPanAngle(Pan_Offset, Pan_0_Position_Mag_Yaw, EastOrWest)
    
    Pan_Angle = -Pan_Angle # Correction for PTU and platform pan/yaw axes being opposite directions (if PTU is hung upside down)
        
    ptuObj.setPosition(Pan_Angle, Tilt_Angle)
        
        
        
        
        