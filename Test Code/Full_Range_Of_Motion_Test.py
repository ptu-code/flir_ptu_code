# -*- coding: utf-8 -*-
"""
Created on Mon May 18 12:11:01 2020
Jeremy Ogorzalek, jeremyo@vt.edu

This 
"""

import time
import numpy as np


# import low level FLIR PTU command class 
#import sys
#sys.path.append("..")
#import ptuSocketBaseClass as psbc
#my_ptu = psbc.PTU(ipAddress = '10.32.40.205')
#my_ptu.setBaseSpeed(0, 0)
#my_ptu.setAcceleration(15, 15)
#my_ptu.setTargetSpeed(10, 10)

# Define the minimum and maximum pan and tilt degrees to test
min_pan = 0
max_pan = 3
min_tilt = 0
max_tilt = 3
# Define degree increments to test
pan_inc = 1
tilt_inc = 1


# method including 0 if possible and going out in both directions from there
# minimum and maximums will always be included

if min_pan <= 0 and max_pan >= 0: # 0 is included
    
    pan_points_neg = []
    i = -pan_inc
    while i > min_pan:
        pan_points_neg.append(i)
        i -= pan_inc
    if len(pan_points_neg) == 0 or pan_points_neg[-1] != min_pan:
        pan_points_neg.append(min_pan)
    pan_points_neg.reverse()
    
    pan_points_pos = []
    i = pan_inc
    while i < max_pan:
        pan_points_pos.append(i)
        i += pan_inc
    if not pan_points_pos[-1] or pan_points_pos[-1] != max_pan:
        pan_points_pos.append(max_pan)
        
    pan_points = pan_points_neg + [0] + pan_points_pos

elif min_pan > 0: # all pan degrees are positive, start at min and increment up
    pan_points_pos = []
    i = min_pan
    while i < max_pan:
        pan_points_pos.append(i)
        i += pan_inc
    if pan_points_pos[-1] != max_pan:
        pan_points_pos.append(max_pan)
    pan_points = pan_points_pos
    
elif max_pan < 0: # all pan degrees are negative, start at max and increment down
    pan_points_neg = []
    i = max_pan
    while i > min_pan:
        pan_points_neg.append(i)
        i -= pan_inc
    if pan_points_neg[-1] != min_pan:
        pan_points_neg.append(min_pan)
    pan_points_neg.reverse()
    pan_points = pan_points_neg
    
    
if min_tilt <= 0 and max_tilt >= 0: # 0 is included
    
    tilt_points_neg = []
    i = -tilt_inc
    while i > min_tilt:
        tilt_points_neg.append(i)
        i -= tilt_inc
    if len(tilt_points_neg) == 0 or tilt_points_neg[-1] != min_tilt:
        tilt_points_neg.append(min_tilt)
    tilt_points_neg.reverse()
    
    tilt_points_pos = []
    i = tilt_inc
    while i < max_tilt:
        tilt_points_pos.append(i)
        i += tilt_inc
    if not tilt_points_pos[-1] or tilt_points_pos[-1] != max_tilt:
        tilt_points_pos.append(max_tilt)
        
    tilt_points = tilt_points_neg + [0] + tilt_points_pos

elif min_tilt > 0: # all tilt degrees are positive, start at min and increment up
    tilt_points_pos = []
    i = min_tilt
    while i < max_tilt:
        tilt_points_pos.append(i)
        i += tilt_inc
    if tilt_points_pos[-1] != max_tilt:
        tilt_points_pos.append(max_tilt)
    tilt_points = tilt_points_pos
    
elif max_tilt < 0: # all tilt degrees are negative, start at max and increment down
    tilt_points_neg = []
    i = max_tilt
    while i > min_tilt:
        tilt_points_neg.append(i)
        i -= tilt_inc
    if tilt_points_neg[-1] != min_tilt:
        tilt_points_neg.append(min_tilt)
    tilt_points_neg.reverse()
    tilt_points = tilt_points_neg
    
# Now we want to combine pan_points and tilt_points into points containing both
C = np.array(np.meshgrid(pan_points, tilt_points)).T.reshape(-1,2).tolist()

Citer = np.linspace(0,len(C)-1, len(C)).reshape(-1,1).astype(int)
CiterT = Citer.T

# Now we want to create list of all possible point pairs
D = np.array(np.meshgrid(Citer, CiterT)).T.reshape(-1,2).tolist()

# there is still some filtering to be done
test_pairs = []
seen = []

for i in D:
    point1iter = i[0]
    point2iter = i[1]
    if point1iter == point2iter:
        pass # point1 is the same as point2, so there is nothing to test
    else:
        point1 = C[point1iter]
        point2 = C[point2iter]
        test_pair = [point1, point2]
        srtd = sorted(test_pair) # no repeat pairs
        if srtd not in seen:
            seen.append(srtd)
            test_pairs.append(test_pair)
        