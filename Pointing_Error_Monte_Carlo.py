# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 14:58:17 2020

@author: JPO
"""

import numpy as np
np.random.seed() # sets seed psuedo-randomly (based on system clock??)
import matplotlib.pyplot as plt

# Set the number of samples you wish to run
# 10,000 is likely sufficient to generate an accurate distribution
num_samples = 10000

# Intended position is [0,0,0]
# Intended pointing vector will be [1,0,0]
# Determine how far away the intended target is:
Dist2Target1 = 10000 # [m]
Dist2Target2 = 1000 # [m]
Dist2Target3 = 100 # [m]

# Function to take in three angles and produce a rotation matrix
def RPY_rotation_matrix(roll, pitch, yaw):
    
    roll = roll*np.pi/180
    pitch = pitch*np.pi/180
    yaw = yaw*np.pi/180
    
    R_yaw = np.array([[np.cos(yaw), -np.sin(yaw), 0],
                      [np.sin(yaw), np.cos(yaw), 0],
                      [0, 0, 1]])or 
    
    R_pitch = np.array([[np.cos(pitch), 0, np.sin(pitch)],
                        [0, 1, 0],
                        [-np.sin(pitch), 0, np.cos(pitch)]])
    
    R_roll = np.array([[1, 0, 0],
                       [0, np.cos(roll), -np.sin(roll)],
                       [0, np.sin(roll), np.cos(roll)]])
    
    return np.matmul(R_roll, np.matmul(R_pitch, R_yaw))
    

'''
Define the parameters defining the accuracy/precision of each link in the pointing chain
'''

# PLatform variables
# Currently using info for VectorNav VN-300, static accuracy
# https://www.vectornav.com/products/vn-300/specifications
# reported in RMS which is the same as standard deviation

imu_roll_measurement_std_dev = .3 # [deg]
imu_pitch_measurement_std_dev = .3 # [deg]
imu_yaw_measurement_std_dev = .5 # [deg]
imu_angular_resolution = .05 # [deg]
imu_alignment_error = .05 # [deg]

imu_roll_rate_std_dev = .4145 # [deg/s]
imu_pitch_rate_std_dev = .5145 # [deg/s]
imu_yaw_rate_std_dev = 7.0593 # [deg/s]

imu_north_measurement_std_dev = .7006 # [m]
imu_east_measurement_std_dev = 1.4670 # [m]
imu_down_measurement_std_dev = .2414 # [m]
imu_position_resolution = .001 # [m]

imu_horizontal_rate_std_dev = 1 # [m/s]
imu_vertical_rate_std_dev = 1 # [m/s]

imu_measurement_update_rate = 400 # [Hz] [1/sec]


# PTU variables
ptu_roll_alignment_std_dev = 1 # [deg]
ptu_pitch_alignment_std_dev = 1 # [deg]
ptu_yaw_alignment_std_dev = 2 # [deg]

ptu_pan_resolution = .006 # [deg]
ptu_tilt_resolution = .003 # [deg]

ptu_x_alignment_std_dev = .02 # [m]
ptu_y_alignment_std_dev = .02 # [m]
ptu_z_alignment_std_dev = .02 # [m]

ptu_command_update_rate = 10 # [Hz] / [1/sec]


# Sensor variables
sensor_pan_alignment_std_dev = 1 # [deg]
sensor_tilt_pitch_alignment_std_dev = .5 # [deg]
sensor_cross_alignment_std_dev = .5 # [deg]

sensor_x_alignment_std_dev = .005
sensor_y_alignment_std_dev = .005
sensor_z_alignment_std_dev = .001


'''
# define starting vector and location
# roll axis aligned with x, pitch aligned with y, yaw aligned with z
'''
initial_vector = np.array([[1],[0],[0]])
initial_position = np.array([[0],[0],[0]]) # [m]
# initially, ptu pan aligned with imu yaw, ptu tilt aligned with imu pitch
target_position1 = np.array([[0+Dist2Target1],[0],[0]]) # [m]
target_position2 = np.array([[0+Dist2Target2],[0],[0]]) # [m]
target_position3 = np.array([[0+Dist2Target3],[0],[0]]) # [m]

'''
# sample random variables
'''

# Platform
imu_roll_measurement_error = np.random.normal(loc=0, scale=imu_roll_measurement_std_dev, size=num_samples)
imu_pitch_measurement_error = np.random.normal(loc=0, scale=imu_pitch_measurement_std_dev, size=num_samples)
imu_yaw_measurement_error = np.random.normal(loc=0, scale=imu_yaw_measurement_std_dev, size=num_samples)

imu_roll_resolution_error = np.random.uniform(low=-imu_angular_resolution/2, high=imu_angular_resolution/2, size=num_samples)
imu_pitch_resolution_error = np.random.uniform(low=-imu_angular_resolution/2, high=imu_angular_resolution/2, size=num_samples)
imu_yaw_resolution_error = np.random.uniform(low=-imu_angular_resolution/2, high=imu_angular_resolution/2, size=num_samples)

imu_roll_alignment_error = np.random.uniform(low=-imu_alignment_error, high=imu_alignment_error, size=num_samples)
imu_pitch_alignment_error = np.random.uniform(low=-imu_alignment_error, high=imu_alignment_error, size=num_samples)
imu_yaw_alignment_error = np.random.uniform(low=-imu_alignment_error, high=imu_alignment_error, size=num_samples)

imu_roll_measurement_lag_error = np.random.normal(loc=0, scale=imu_roll_rate_std_dev, size=num_samples)/imu_measurement_update_rate
imu_pitch_measurement_lag_error = np.random.normal(loc=0, scale=imu_pitch_rate_std_dev, size=num_samples)/imu_measurement_update_rate
imu_yaw_measurement_lag_error = np.random.normal(loc=.0006, scale=imu_yaw_rate_std_dev, size=num_samples)/imu_measurement_update_rate

imu_x_measurement_error = np.random.normal(loc=-3.1572, scale=imu_north_measurement_std_dev, size=num_samples)
imu_y_measurement_error = np.random.normal(loc=8.3635, scale=imu_east_measurement_std_dev, size=num_samples)
imu_z_measurement_error = np.random.normal(loc=.0057, scale=imu_down_measurement_std_dev, size=num_samples)

imu_x_resolution_error = np.random.uniform(low=-imu_position_resolution/2, high=imu_position_resolution/2, size=num_samples)
imu_y_resolution_error = np.random.uniform(low=-imu_position_resolution/2, high=imu_position_resolution/2, size=num_samples)
imu_z_resolution_error = np.random.uniform(low=-imu_position_resolution/2, high=imu_position_resolution/2, size=num_samples)

imu_x_measurement_lag_error = np.random.normal(loc=0, scale=imu_horizontal_rate_std_dev, size=num_samples)/imu_measurement_update_rate
imu_y_measurement_lag_error = np.random.normal(loc=0, scale=imu_horizontal_rate_std_dev, size=num_samples)/imu_measurement_update_rate
imu_z_measurement_lag_error = np.random.normal(loc=0, scale=imu_vertical_rate_std_dev, size=num_samples)/imu_measurement_update_rate

# PTU
ptu_roll_alignment_error = np.random.normal(loc=0, scale=ptu_roll_alignment_std_dev, size=num_samples)
ptu_pitch_alignment_error = np.random.normal(loc=0, scale=ptu_pitch_alignment_std_dev, size=num_samples)
ptu_yaw_alignment_error = np.random.normal(loc=0, scale=ptu_yaw_alignment_std_dev, size=num_samples)

ptu_x_alignment_error = np.random.normal(loc=0, scale=ptu_x_alignment_std_dev, size=num_samples)
ptu_y_alignment_error = np.random.normal(loc=0, scale=ptu_y_alignment_std_dev, size=num_samples)
ptu_z_alignment_error = np.random.normal(loc=0, scale=ptu_z_alignment_std_dev, size=num_samples)

ptu_pan_resolution_error = np.random.uniform(low=-ptu_pan_resolution/2, high=ptu_pan_resolution/2, size=num_samples)
ptu_tilt_resolution_error = np.random.uniform(low=-ptu_tilt_resolution/2, high=ptu_tilt_resolution/2, size=num_samples)

ptu_pan_update_lag_error = np.random.normal(loc=0, scale=imu_yaw_rate_std_dev, size=num_samples)/ptu_command_update_rate
ptu_tilt_update_lag_error = np.random.normal(loc=0, scale=imu_yaw_rate_std_dev, size=num_samples)/ptu_command_update_rate

# Sensor
sensor_pan_alignment_error = np.random.normal(loc=0, scale=ptu_roll_alignment_std_dev, size=num_samples)
sensor_tilt_alignment_error = np.random.normal(loc=0, scale=ptu_pitch_alignment_std_dev, size=num_samples)
sensor_cross_alignment_error = np.random.normal(loc=0, scale=ptu_yaw_alignment_std_dev, size=num_samples)

sensor_x_alignment_error = np.random.normal(loc=0, scale=sensor_x_alignment_std_dev, size=num_samples)
sensor_y_alignment_error = np.random.normal(loc=0, scale=sensor_y_alignment_std_dev, size=num_samples)
sensor_z_alignment_error = np.random.normal(loc=0, scale=sensor_z_alignment_std_dev, size=num_samples)


# Initialize vectors
platform_vectors = []
platform_translations = []
ptu_vectors = []
ptu_translations = []
sensor_vectors = []
sensor_translations = []

absolute_angular_errors = []
relative_angular_errors1 = []
relative_angular_errors2 = []
relative_angular_errors3 = []

# TODO - determine if all of these errors should be successive rotations, or should some of them simply be added?
# TODO - calculate both ways and see if there is any difference

for i in range(num_samples):
    
    # Determine rotation matrix that rotates the intended pointing vector
    # to the true pointing vector caused by platform attitude errors
    imu_measurement_error_R = RPY_rotation_matrix(imu_roll_measurement_error[i], imu_pitch_measurement_error[i], imu_yaw_measurement_error[i])
    imu_resolution_error_R = RPY_rotation_matrix(imu_roll_resolution_error[i], imu_pitch_resolution_error[i], imu_yaw_resolution_error[i])
    imu_alignment_error_R = RPY_rotation_matrix(imu_roll_alignment_error[i], imu_pitch_alignment_error[i], imu_yaw_alignment_error[i])
    imu_measurement_lag_error_R = RPY_rotation_matrix(imu_roll_measurement_lag_error[i], imu_pitch_measurement_lag_error[i], imu_yaw_measurement_lag_error[i])
    platform_R = np.matmul(imu_measurement_lag_error_R, np.matmul(imu_alignment_error_R, np.matmul(imu_resolution_error_R, imu_measurement_error_R)))
    
    # Rotate the intended vector to get the eroneous vector caused by platform attitude error
    platform_vector = np.matmul(platform_R, initial_vector)
    platform_vectors.append(platform_vector)
    
    # Determine the translation caused by platform position error
    platform_x = imu_x_measurement_error[i] + imu_x_resolution_error[i] + imu_x_measurement_lag_error[i]
    platform_y = imu_y_measurement_error[i] + imu_y_resolution_error[i] + imu_y_measurement_lag_error[i]
    platform_z = imu_z_measurement_error[i] + imu_z_resolution_error[i] + imu_z_measurement_lag_error[i]
    
    # Translate the intended position to get the eroneous position caused by platform position error
    platform_translation = np.array([[platform_x],[platform_y],[platform_z]])
    platform_translations.append(platform_translation)
    
    # Determine rotation matrix that rotates the intended pointing vector
    # to the true pointing vector caused by PTU pointing errors
    ptu_alignment_error = RPY_rotation_matrix(ptu_roll_alignment_error[i], ptu_roll_alignment_error[i], ptu_roll_alignment_error[i])
    ptu_resolution_error = RPY_rotation_matrix(0, ptu_tilt_resolution_error[i], ptu_pan_resolution_error[i])
    ptu_update_lag_error = RPY_rotation_matrix(0, ptu_tilt_update_lag_error[i], ptu_pan_update_lag_error[i])
    ptu_R = np.matmul(ptu_update_lag_error, np.matmul(ptu_resolution_error, ptu_alignment_error))
    
    # Rotate the erroneous platform vector by the PTU rotation matrix to get the
    # erroneous ptu vector
    ptu_vector = np.matmul(ptu_R, platform_vector)
    ptu_vectors.append(ptu_vector)
    
    # Translating the PTU is a little more difficult, as the translation offset of the PTU
    # is with respect to the platform coordinates, not the global coordinates
    # We must rotate the PTU position errors by the platform rotation matrix
    # before adding it to the platform position errors to get the true
    # PTU translation
    ptu_translation_relative_local = np.array([[ptu_x_alignment_error[i]], [ptu_y_alignment_error[i]], [ptu_z_alignment_error[i]]])
    ptu_translation_relative_global = np.matmul(platform_R, ptu_translation_relative_local)
    ptu_translation = ptu_translation_relative_global + platform_translation
    ptu_translations.append(ptu_translation)
    
    # Determine the rotation matrix resulting from the sensor angular alignment errors
    sensor_R = RPY_rotation_matrix(sensor_cross_alignment_error[i], sensor_tilt_alignment_error[i], sensor_pan_alignment_error[i])
    
    # Rotate the ptu_vectors from above to get the sensor vectors
    sensor_vector = np.matmul(sensor_R, ptu_vector)
    sensor_vectors.append(sensor_vector)
    
    # Translating the sensor is relative to the PTU rotation
    sensor_translation_relative_local = np.array([[sensor_x_alignment_error[i]], [sensor_y_alignment_error[i]], [sensor_z_alignment_error[i]]])
    sensor_translation_relative_global = np.matmul(ptu_R, sensor_translation_relative_local)
    sensor_translation = sensor_translation_relative_global + ptu_translation
    sensor_translations.append(sensor_translation)
    
    # Determine the errors
    absolute_angular_error = np.arccos(np.dot(initial_vector.T, sensor_vector) / (np.linalg.norm(initial_vector)*np.linalg.norm(sensor_vector)))*180/np.pi
    absolute_angular_errors.append(absolute_angular_error[0][0])
    
    # Determine new position offset and therefore intended vector
    new_vector1 = target_position1 - sensor_translation
    relative_angular_error1 = np.arccos(np.dot(new_vector1.T, sensor_vector) / (np.linalg.norm(new_vector1)*np.linalg.norm(sensor_vector)))*180/np.pi
    relative_angular_errors1.append(relative_angular_error1[0][0])
    
    new_vector2 = target_position2 - sensor_translation
    relative_angular_error2 = np.arccos(np.dot(new_vector2.T, sensor_vector) / (np.linalg.norm(new_vector2)*np.linalg.norm(sensor_vector)))*180/np.pi
    relative_angular_errors2.append(relative_angular_error2[0][0])
    
    new_vector3 = target_position3 - sensor_translation
    relative_angular_error3 = np.arccos(np.dot(new_vector3.T, sensor_vector) / (np.linalg.norm(new_vector3)*np.linalg.norm(sensor_vector)))*180/np.pi
    relative_angular_errors3.append(relative_angular_error3[0][0])
    
    if i % 100 == 0:
        print(i)

mean_abs_errors = np.mean(absolute_angular_errors)
mean_rel_errors1 = np.mean(relative_angular_errors1)
mean_rel_errors2 = np.mean(relative_angular_errors2)
mean_rel_errors3 = np.mean(relative_angular_errors3)

std_dev_abs_errors = np.std(absolute_angular_errors)
std_dev_rel_errors1 = np.std(relative_angular_errors1)
std_dev_rel_errors2 = np.std(relative_angular_errors2)
std_dev_rel_errors3 = np.std(relative_angular_errors3)

five_percentile_abs_errors = np.percentile(absolute_angular_errors, 5)
fifty_percentile_abs_errors = np.percentile(absolute_angular_errors, 50)
ninetyfive_percentile_abs_errors = np.percentile(absolute_angular_errors, 95)
five_percentile_rel_errors1 = np.percentile(relative_angular_errors1, 5)
fifty_percentile_rel_errors1 = np.percentile(relative_angular_errors1, 50)
ninetyfive_percentile_rel_errors1 = np.percentile(relative_angular_errors1, 95)
five_percentile_rel_errors2 = np.percentile(relative_angular_errors2, 5)
fifty_percentile_rel_errors2 = np.percentile(relative_angular_errors2, 50)
ninetyfive_percentile_rel_errors2 = np.percentile(relative_angular_errors2, 95)
five_percentile_rel_errors3 = np.percentile(relative_angular_errors3, 5)
fifty_percentile_rel_errors3 = np.percentile(relative_angular_errors3, 50)
ninetyfive_percentile_rel_errors3 = np.percentile(relative_angular_errors3, 95)

fig, axs = plt.subplots(1, 4, sharey=True)
fig.text(0.5, 0.02, 'Angular Pointing Error [deg]', ha='center', fontsize=18)
fig.text(0.06, 0.5, 'Histogram Bin Count', va='center', rotation='vertical', fontsize=18)
fig.subplots_adjust(wspace=.1)

from matplotlib.lines import Line2D
legend_elements = [Line2D([0], [0], color='red', label='Mean'),
                   Line2D([0], [0], color='red', label='+/- Std Dev', linestyle='--'),
                   Line2D([0], [0], color='orange', label='50%'),
                   Line2D([0], [0], color='orange', label='5% / 95%', linestyle='--')]
fig.legend(handles=legend_elements, loc='upper left', bbox_to_anchor=(.02, .85))

axs[0].hist(absolute_angular_errors, bins=100)
axs[0].axvline(x=mean_abs_errors, color='red')
axs[0].axvline(x=mean_abs_errors-std_dev_abs_errors, color='red', linestyle='--')
axs[0].axvline(x=mean_abs_errors+std_dev_abs_errors, color='red', linestyle='--')
axs[0].axvline(x=fifty_percentile_abs_errors, color='orange')
axs[0].axvline(x=five_percentile_abs_errors, color='orange', linestyle='--')
axs[0].axvline(x=ninetyfive_percentile_abs_errors, color='orange', linestyle='--')
axs[0].set_title('Absolute Angle Error')

legend_elements0 = [Line2D([0], [0], color='red', label=str(round(mean_abs_errors,2))),
                   Line2D([0], [0], color='red', label='+/-'+str(round(std_dev_abs_errors,2)), linestyle='--'),
                   Line2D([0], [0], color='orange', label=str(round(fifty_percentile_abs_errors,2))),
                   Line2D([0], [0], color='orange', label=str(round(five_percentile_abs_errors,2)) + ' / ' + str(round(ninetyfive_percentile_abs_errors,2)), linestyle='--')]
axs[0].legend(handles=legend_elements0, loc='upper right')

axs[1].hist(relative_angular_errors1, bins=100)
axs[1].axvline(x=mean_rel_errors1, color='red')
axs[1].axvline(x=mean_rel_errors1-std_dev_rel_errors1, color='red', linestyle='--')
axs[1].axvline(x=mean_rel_errors1+std_dev_rel_errors1, color='red', linestyle='--')
axs[1].axvline(x=fifty_percentile_rel_errors1, color='orange')
axs[1].axvline(x=five_percentile_rel_errors1, color='orange', linestyle='--')
axs[1].axvline(x=ninetyfive_percentile_rel_errors1, color='orange', linestyle='--')
axs[1].set_title('Relative Angle Error at 10 km')

legend_elements1 = [Line2D([0], [0], color='red', label=str(round(mean_rel_errors1,2))),
                   Line2D([0], [0], color='red', label='+/-'+str(round(std_dev_rel_errors1,2)), linestyle='--'),
                   Line2D([0], [0], color='orange', label=str(round(fifty_percentile_rel_errors1,2))),
                   Line2D([0], [0], color='orange', label=str(round(five_percentile_rel_errors1,2)) + ' / ' + str(round(ninetyfive_percentile_rel_errors1,2)), linestyle='--')]
axs[1].legend(handles=legend_elements1, loc='upper right')

axs[2].hist(relative_angular_errors2, bins=100)
axs[2].axvline(x=mean_rel_errors2, color='red')
axs[2].axvline(x=mean_rel_errors2-std_dev_rel_errors2, color='red', linestyle='--')
axs[2].axvline(x=mean_rel_errors2+std_dev_rel_errors2, color='red', linestyle='--')
axs[2].axvline(x=fifty_percentile_rel_errors2, color='orange')
axs[2].axvline(x=five_percentile_rel_errors2, color='orange', linestyle='--')
axs[2].axvline(x=ninetyfive_percentile_rel_errors2, color='orange', linestyle='--')
axs[2].set_title('Relative Angle Error at 1 km')

legend_elements2 = [Line2D([0], [0], color='red', label=str(round(mean_rel_errors2,2))),
                   Line2D([0], [0], color='red', label='+/-'+str(round(std_dev_rel_errors2,2)), linestyle='--'),
                   Line2D([0], [0], color='orange', label=str(round(fifty_percentile_rel_errors2,2))),
                   Line2D([0], [0], color='orange', label=str(round(five_percentile_rel_errors2,2)) + ' / ' + str(round(ninetyfive_percentile_rel_errors2,2)), linestyle='--')]
axs[2].legend(handles=legend_elements2, loc='upper right')

axs[3].hist(relative_angular_errors3, bins=100)
axs[3].axvline(x=mean_rel_errors3, color='red')
axs[3].axvline(x=mean_rel_errors3-std_dev_rel_errors3, color='red', linestyle='--')
axs[3].axvline(x=mean_rel_errors3+std_dev_rel_errors3, color='red', linestyle='--')
axs[3].axvline(x=fifty_percentile_rel_errors3, color='orange')
axs[3].axvline(x=five_percentile_rel_errors3, color='orange', linestyle='--')
axs[3].axvline(x=ninetyfive_percentile_rel_errors3, color='orange', linestyle='--')
axs[3].set_title('Relative Angle Error at 0.1 km')

legend_elements3 = [Line2D([0], [0], color='red', label=str(round(mean_rel_errors3,2))),
                   Line2D([0], [0], color='red', label='+/-'+str(round(std_dev_rel_errors3,2)), linestyle='--'),
                   Line2D([0], [0], color='orange', label=str(round(fifty_percentile_rel_errors3,2))),
                   Line2D([0], [0], color='orange', label=str(round(five_percentile_rel_errors3,2)) + ' / ' + str(round(ninetyfive_percentile_rel_errors3,2)), linestyle='--')]
axs[3].legend(handles=legend_elements3, loc='upper right')



'''
to get angle [degrees] between vectors a and b
c = np.dot(a.T, b)
d = c / (np.linalg.norm(a)*np.linalg.norm(b))
e = np.arccos(d)
f = e*180/np.pi
'''



'''
# test of rotations and translations
platform_R = RPY_rotation_matrix(0,0,90)
platform_translation = np.array([[0],[0],[0]])
ptu_translation_relative_local = np.array([[1],[0],[0]])
ptu_translation_relative_global = np.matmul(platform_R, ptu_translation_relative_local)
ptu_translation = ptu_translation_relative_global + platform_translation
'''