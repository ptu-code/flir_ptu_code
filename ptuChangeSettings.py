# -*- coding: utf-8 -*-

'''
This file can be used to set basic settings on the PTU
'''

import ptuSocketBaseClass as ptbc
my_ptu = ptbc.PTU(ipAddress='10.32.40.205')

# set execution mode to 'I' for 'instantaneous' so that ptu moves as soon as command is recieved
# default is 'slaved' mode where ptu waits for .Await() command before moving
my_ptu.setExecutionMode('I')

# set step mode to 'E' for 'eighth' for both pan and tilt
# in this mode, the ptu has the finest level of control and the maximum torque
my_ptu.setStepMode('E', 'E')
# can also set to 'A' for automatic

# set moving power to regular instead of low. high is also possible, but can damage
# the ptu, and so is not programmed as an option in ptuSocketBaseClass
my_ptu.setMovingPower('regular', 'regular')

# set the speed at which the ptu performs it's recalibration procedure, both
# when the ptu is reset or when commanded to zero it's axes
my_ptu.setResetSpeed(10, 10)

# set base speed, target speed, and acceleration
my_ptu.setBaseSpeed(0, 0) # start at 0 deg/s to give maximum torque
my_ptu.setTargetSpeed(10, 10) # maximum speed for ptu to move at
my_ptu.setAcceleration(15, 15) # lower gives more torque. 15 deg/s^2 is about the lowest possible

my_ptu.getBaseSpeed()
my_ptu.getTargetSpeed()
my_ptu.getAcceleration()

# if desired, set and enable position limits
#my_ptu.setUserPanLimits(minimum_pan_deg, maximum_pan_deg)
#my_ptu.setUserTiltLimits(minimum_tilt_deg, maximum_tilt_deg)
#my_ptu.enableUserLimits()

# if desired, enable continuous rotation mode
# WARNING: be very careful about any cable that may become tangled
#my_ptu.setContRotation('on')

# save settings so that remain in memory and remain after power down
my_ptu.saveSettings()

my_ptu.resetPTU()


























