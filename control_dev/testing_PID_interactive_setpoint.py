# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 12:11:01 2020

@author: jeremyo
"""
import time
import numpy as np

# import low level FLIR PTU command class 
import sys
sys.path.append("..")
import ptuSocketBaseClass as psbc
# connect to PTU and set to velocity control mode
my_ptu = psbc.PTU(ipAddress = '10.32.40.205')
my_ptu.setControlMode('vel')

loop_time = .1 # how long to pause at the end of each loop, seconds
seconds2plot = 10 # how many seconds to plot

# set gains!
Kp = 2
Ki = 1
Kd = .001


loops2plot = seconds2plot/loop_time



errors = [0]
times = [time.time()]

# use below for established setpoint time history
#desired_pan_position = [0]*50 + [5]*1950

# use below to interactively change the setpoint
import keyboard
desired_pan_positions = [0]
setpoint = 0

current_pan_position = [0]


def PID_output(measured, setpoint, previous_times, previous_errors):
    
    error = setpoint - measured
    current_time = time.time()
    
#    print('current_time: ', str(current_time))
    
    errors.append(error)
    
#    print('previous_times[-1]: ', str(previous_times[-1]))
    
    timestep = current_time - previous_times[-1]
    
    times.append(current_time)
    
    P_term = Kp * error
    
    I_term = Ki * np.trapz(errors, times)
    
    D_term = Kd * (error - previous_errors[-1])/timestep
    
    return P_term + I_term + D_term


i = 1
print("Initial setpoint is " + str(setpoint))

import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111)
Ln1, = ax.plot(range(i), desired_pan_positions)
Ln2, = ax.plot(range(i), current_pan_position)
plt.ion()
plt.show()


while True:
    
    if len(times) > 100:
        previous_times = times[-100:]
        previous_errors = errors[-100:]
    else:
        previous_times = times
        previous_errors = errors
    
    desired_pan_positions.append(setpoint)
    
    measured_pan = my_ptu.getPanPosition()
    current_pan_position.append(measured_pan)
    
    pan_velocity = PID_output(measured_pan, setpoint, previous_times, previous_errors)
    
    if pan_velocity > 10:
        pan_velocity = 9.99
    elif pan_velocity < -10:
        pan_velocity = -9.99
        
    print('desired, measured, speed: ' + str(setpoint) + ', ' + str(measured_pan) + ', ' + str(pan_velocity))

        
    my_ptu.setPanTargetSpeed(pan_velocity)
    
    i += 1
    
    if i >= loops2plot:
        Ln1, = ax.plot(range(i)[-loops2plot:], desired_pan_positions[-loops2plot:])
        Ln2, = ax.plot(range(i)[-loops2plot:], current_pan_position[-loops2plot:])
    else: 
        Ln1, = ax.plot(range(i), desired_pan_positions)
        Ln2, = ax.plot(range(i), current_pan_position)
        
    plt.pause(loop_time)
#    time.sleep(.1)
    
    if keyboard.is_pressed("w"):
        setpoint += 5
        print("Setpoint increased to " + str(setpoint))
    if keyboard.is_pressed("s"):
        setpoint -= 5
        print("Setpoint decreased to " + str(setpoint))
    elif keyboard.is_pressed("q"):
        print("quitting")
        break
    
my_ptu.setPanTargetSpeed(0)
    
    
    
    
    
    
    
    
    
    
    
    
    
    