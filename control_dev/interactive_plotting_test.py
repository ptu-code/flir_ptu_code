# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 11:37:14 2020

@author: jeremyo
"""

import pylab
import time
import random
import matplotlib.pyplot as plt

dat=[0,1]
fig = plt.figure()
ax = fig.add_subplot(111)
Ln, = ax.plot(dat)
ax.set_xlim([0,20])
plt.ion()
plt.show()

for i in range (18):
    dat.append(random.uniform(0,1))
    Ln.set_ydata(dat)
    Ln.set_xdata(range(len(dat)))
    plt.pause(1)





desired_pan_positions = [0]
current_pan_position = [0]

i = 0

import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111)
Ln1, = ax.plot(range(i+1), desired_pan_positions, 'b', label='Setpoint')
Ln2, = ax.plot(range(i+1), current_pan_position, 'r', label='Current')
plt.ion()
ax.legend()
plt.show()



for n in range(10):
    
    i += 1
    
    desired_pan_positions.append(i)
    current_pan_position.append(i+2)
    
    Ln1, = ax.plot(range(i+1), desired_pan_positions)
    Ln2, = ax.plot(range(i+1), current_pan_position)
    plt.pause(1)