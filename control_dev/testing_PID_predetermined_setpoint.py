# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 12:11:01 2020

@author: jeremyo
"""
import time
import numpy as np

import sys
sys.path.append("..")
import ptuSocketBaseClass as psbc
my_ptu = psbc.PTU(ipAddress = '10.32.40.200')

Kp = 2
Ki = 1
Kd = .001

desired_pan_position = [0]*50 + [5]*1950

errors = [0]
times = [time.time()]

current_pan_position = []

def PID_output(measured, setpoint, previous_times, previous_errors):
    
    error = setpoint - measured
    current_time = time.time()
    
    errors.append(error)
    times.append(current_time)
    
    timestep = current_time - previous_times[-1]
    
    P_term = Kp * error
    
    I_term = Ki * np.trapz(errors, times)
    
    D_term = Kd * (error - previous_errors[-1])/timestep
    
    return P_term + I_term + D_term


i = 0

while True:
    
    if len(times) > 100:
        previous_times = times[-100:]
        previous_errors = errors[-100:]
    else:
        previous_times = times
        previous_errors = errors
    
    setpoint = desired_pan_position[i]
    
    measured_pan = my_ptu.getPanPosition()
    
    pan_velocity = PID_output(measured_pan, setpoint, previous_times, previous_errors)
    
    my_ptu.setPanSpeed(pan_velocity)
    
    i += 1
    time.sleep(.25)

    if i == len(desired_pan_position):
        break
    
    
    
    
    
    
    
    
    
    
    
    
    
    