# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 12:58:55 2019

@author: JPO
"""

import newPTUclassSocket as ptuSock
import numpy as np
# import time

# Define several constants
deg2rad = np.pi/180
rad2deg = 180/np.pi
R_e = 6378.137  # Earth Equitorial Radius [km]
e_e = 0.08181979099211309 # Eccentricity of Earth

# TODO - For every single function, should we do a check to make sure we will stay within pan and tilt limits set by user?

def connect2ptu():
# Call to create the PTU object and connect via ethernet socket
# Inputs:
# Outputs:
    # MyPTU: Name of the PTU object
    MyPTU = ptuSock.PTU()
    MyPTU.setExecutionMode('I') # Set execution mode to instantaneous
    return MyPTU

def SetSpeeds(BaseSpeed, Acceleration, TargetSpeed, ptuObj):
# Set the base speed, acceleration, and target speed of the PTU
# Inputs:
    # BaseSpeed [deg/s]
    # Acceleration [deg/s^2]
    # TargetSpeed [deg/s]
    # ptuObj
# Outputs:
    ptuObj.setBaseSpeed(BaseSpeed, BaseSpeed)
    ptuObj.setAcceleration(Acceleration, Acceleration)
    ptuObj.setTargetSpeed(TargetSpeed, TargetSpeed)
    
def GetSpeeds(ptuObj):
# Get the base speed, acceleration, and target speed of the PTU
# Inputs:
# Outputs:
    # BasePanSpeed, BaseTiltSpeed [deg/s]
    # PanAcceleration, TiltAcceleration [deg/s^2]
    # TargetPanSpeed, TargetTiltSpeed [deg/s]
    return ptuObj.getBaseSpeed(), ptuObj.getAcceleration(), ptuObj.getTargetSpeed()
    
def SetPositionLimits(MinTilt, MaxTilt, MinPan, MaxPan, ptuObj):
# Set the anglar position limits of the PTU
# Inputs:
    # MinTilt [deg]
    # MaxTilt [deg]
    # MinPan [deg]
    # MaxPan [deg]
    # ptuObj [deg]
# Outputs:
    ptuObj.setUserPanLimits(MinTilt, MaxTilt)
    ptuObj.setUserTiltLimits(MinPan, MaxPan)
    ptuObj.enableUserLimits()
    
def GetPositionLimits(ptuObj):
# Get the anglar position limits of the PTU
# Inputs:
    # ptuObj: Name of the PTU object
# Outputs:
    # MinTilt, MaxTilt [deg]
    # MinPan, MaxPan [deg]
    return ptuObj.getUserPanLimits(), ptuObj.getUserTiltLimits()

def ZeroZero(ptuObj):
# Send the ptu to 0 pan, 0 tilt
# Inputs:
    # ptuObj: Name of the PTU object
    ptuObj.setPosition(0, 0)
    ptuObj.Await()

def Store(ptuObj):
# Send the ptu to 0 pan, minimum tilt
# Inputs:
    # ptuObj: Name of the PTU object
    ptuObj.setPosition(0, ptuObj.minTilt)
    ptuObj.Await()
    
def GetPosition(ptuObj):
# Get the current anglar positions of the PTU
# Inputs:
    # ptuObj: Name of the PTU object
# Outputs:
    # Pan, Tilt [deg]

    Pan, Tilt = ptuObj.getPosition()
    return Pan, Tilt
    
def LLH_To_ECEF(lat, lon, h):
# Converts a LLH/LLA position to an ECEF position vector
# Inputs:
	# h   - height above ellipsoid (MSL), km
	# lat - geodetic latitude, [deg]
	# lon - geodetic longitude, [deg]
 # Outputs:
    # ECEF X, Y, Z [km]
    
    lat = lat*deg2rad
    lon = lon*deg2rad
    
    N_e = R_e / np.sqrt(1 - (e_e**2) * (np.sin(lat)**2))
    BoverA = 1 - e_e**2
    X = (N_e + h) * np.cos(lat) * np.cos(lon)
    Y = (N_e + h) * np.cos(lat) * np.sin(lon)
    Z = (BoverA * N_e + h) * np.sin(lat)
    return X, Y, Z

def RotMat(theta, angleNum):
# Creates Euler angle rotation matrices
# Inputs:
    # theta: angle to rotate [rads]
    # angleNum: 1 for rotation about x axis, 2 for about y axis, 3 for about z axis
# Outputs:
    # R: 3x3 rotation matrix 
    
    assert angleNum == 1 or angleNum == 2 or angleNum == 3
    
    while theta > 360:
        theta = theta - 360
    
    if angleNum == 1:
        R = np.matrix([[1,0,0],[0, np.cos(theta),np.sin(theta)],[0,-1*np.sin(theta),np.cos(theta)]])
    elif angleNum == 2:
        R = np.matrix([[np.cos(theta),0,-1*np.sin(theta)],[0,1,0],[np.sin(theta),0,np.cos(theta)]])
    elif angleNum == 3:
        R = np.matrix([[np.cos(theta),np.sin(theta),0],[-1*np.sin(theta),np.cos(theta),0],[0,0,1]])
    return R

def FromGPS2GPS_Angles(platform_LLA, Target_LLA, platformRPY_NED):
# Calculates the Pan and Tilt angles required to point at a target location
# Inputs:
    # platform_LLA: [Latitude, Longitude, Altitude] of ptu in [deg, deg, km]
    # Target_LLA: [Latitude, Longitude, Altitude] of target in [deg, deg, km]
    # platformRPY_NED: PLatform roll, pitch, and yaw angles [Euler angles] from the local North/East/Down coordinate system [deg, deg, deg]
# Outputs:
    # Pan_Angle: Pan angle to point ptu at target
    # Tilt_Angle: Tilt angle to point ptu at target (measured from horizontal down, ie. 90 degrees is straight down)
    
    platformLatitude = platform_LLA[0]
    platformLongitude = platform_LLA[1]
    platformAltitude = platform_LLA[2]
    TargetLatitude = Target_LLA[0]
    TargetLongitude = Target_LLA[1]
    TargetAltitude = Target_LLA[2]

    r_platform_ECEF   = np.array(LLH_To_ECEF(platformLatitude, platformLongitude, platformAltitude))
    r_Target_ECEF   = np.array(LLH_To_ECEF(TargetLatitude, TargetLongitude, TargetAltitude))
    r_platform2Target_ECEF = r_Target_ECEF - r_platform_ECEF
    
    lat1 = platformLatitude*deg2rad
    lon1 = platformLongitude*deg2rad
    
    RotMat_ECEF2NED = np.array([[-np.sin(lat1)*np.cos(lon1), -np.sin(lat1)*np.sin(lon1),  np.cos(lat1)],
                                [-np.sin(lon1)             ,  np.cos(lon1)             ,  0           ],
                                [-np.cos(lat1)*np.cos(lon1), -np.cos(lat1)*np.sin(lon1), -np.sin(lat1)]])

    r_platform2Target_NED = np.matmul(RotMat_ECEF2NED,r_platform2Target_ECEF)
    
#    r_platform_NED = np.matmul(RotMat_ECEF2NED,r_platform_ECEF)
#    r_Target_NED = np.matmul(RotMat_ECEF2NED,r_Target_ECEF)
#    r_platform2Target_NED2 = r_Target_NED-r_platform_NED
    
    roll = platformRPY_NED[0]*deg2rad
    pitch = platformRPY_NED[1]*deg2rad
    yaw = platformRPY_NED[2]*deg2rad
        
    RotMat_NED2Body = np.matmul(RotMat(roll, 1),np.matmul(RotMat(pitch, 2),RotMat(yaw, 3)))
    r_platform2Target_Body = np.matmul(RotMat_NED2Body, r_platform2Target_NED)
    Pan_Angle = np.arctan2(r_platform2Target_Body[0,1], r_platform2Target_Body[0,0]) * rad2deg
    Tilt_Angle = np.arctan2(r_platform2Target_Body[0,2], np.sqrt(r_platform2Target_Body[0,1]**2 + r_platform2Target_Body[0,0]**2)) * rad2deg
    
    return Pan_Angle, Tilt_Angle

def FromGPS2GPS_Point(platform_LLA, Target_LLA, platformRPY_NED, ptuObj):
# Commands the ptu to point at the specified target location
# Inputs:
    # platform_LLA: [Latitude, Longitude, Altitude] of platform in [deg, deg, km]
    # Target_LLA: [Latitude, Longitude, Altitude] of target in [deg, deg, km]
    # platformRPY_NED: platform roll, pitch, and yaw angles [Euler angles] from the local North/East/Down coordinate system [deg, deg, deg]
    # AngularSpeed: Target/Operational speed to move at [deg/sec]
    # ptuObj: Name of the PTU object
    
    Pan_Angle, Tilt_Angle = FromGPS2GPS_Angles(platform_LLA, Target_LLA, platformRPY_NED)   
    ptuObj.setPosition(Pan_Angle, Tilt_Angle)
    #ptuObj.Await()
    
    
