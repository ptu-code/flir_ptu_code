# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 09:42:00 2019

@author: JPO
"""


import imu
import PointOffMag as pom
import time

rnd = int(5) # What decimal to round angles (deg) to

# Connect to VectorNav IMU and PTU
vec_nav = imu.IMU(com_port='COM12',baudrate=115200) # May need to change COM_Port and baudrate
ptu = pom.connect2ptu()

print(pom.GetSpeeds(ptu))
pom.SetSpeeds(0,15,10,ptu) # Set speeds (base, accel, target)

# Set virtual Magnetic Declination, in terms of difference from true North
# Comment out for real flight
Magnetic_Declination = 0

# Parameters to set before flight

PTU_Orientation = 1 # 1, 2, 3 or 4, as described below
# PTU_Orientation parameter:
# 1: Pan 0 aligned with INS X axis
# 2: Pan 0 aligned with INS Y axis
# 3: Pan 0 aligned with INS -X axis
# 4: Pan 0 aligned with INS -X axis
# Note: Pan 0 is the face opposite the power plug

EastOrWest = 'West'
# 
Tilt_Offset_From_Horizontal = 10 # [deg], Measured down from horizontal
Pan_Offset = 10 # [deg], Measured from magnetic EastOrWest, always towards magnetic South!


#pom.ZeroZero(ptu)
#
#pom.Store(ptu)
#
#pom.ZeroZero(ptu)

while True:

    vec_nav.grab_ypr() # Get Yaw, Pitch, Roll from VectorNav
    Yaw = round(vec_nav.ypr.x, rnd)
    Pitch = round(vec_nav.ypr.y, rnd)
    Roll = round(vec_nav.ypr.z, rnd)
    # This block will be replaces with grabbing same values from INS
    
    # Read in Magnetic Declination
    # Magnetic_Declination = 
    
    if PTU_Orientation == 1:
        Tilt_Correction = Pitch
        Pan_Correction = 0
    elif PTU_Orientation == 2:
        Tilt_Correction = Roll
        Pan_Correction = 90
    elif PTU_Orientation == 3:
        Tilt_Correction = -Pitch
        Pan_Correction = 180
    elif PTU_Orientation == 4:
        Tilt_Correction = -Roll
        PTU_Orienation = 270
    # This block can be removed, and Tilt_Correction and Pan_Correction hard-coded, once the PTU mounting orientation in known
    
    pom.PointOffMag(Tilt_Offset_From_Horizontal, Tilt_Correction, Yaw, Pan_Offset, Pan_Correction, EastOrWest, Magnetic_Declination, ptu)
    
    time.sleep(.1) # Comment out to loop as fast as possible
    
    CurrentPan, CurrentTilt = pom.GetPosition(ptu) # Get current position, will want to save to file
    # Save current pan and tilt values to file along with timestamp
    print(CurrentPan, CurrentTilt)
